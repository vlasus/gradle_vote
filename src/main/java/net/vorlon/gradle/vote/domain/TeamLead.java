/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.vorlon.gradle.vote.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;

/**
 *
 * @author vlasus
 */
@Data
public class TeamLead {
    @Id
    public String id;
    
    Long teamId;
    String participantIdentifier;
    
    public TeamLead(Long teamId,
        String participantIdentifier){
        this.teamId=teamId;
        this.participantIdentifier=participantIdentifier;
    }
}
