/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.vorlon.gradle.vote.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
/**
 *
 * @author vlasus
 */
@Data
public class Participant {
    @Id
    public String id;

    public String firstName;
    public String lastName;
    private String identifier;
    
    public Participant(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        StringBuilder buf=new StringBuilder(firstName);
        buf.append(' ').append(lastName);
        identifier=buf.toString();
        }
    
    @Override
    public String toString() {
        return String.format(
                "Participant[id=%s, firstName='%s', lastName='%s']",
                id, firstName, lastName);
    }
}
