/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.vorlon.gradle.vote.queue;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 *
 * @author vlasus
 */
@Data
@AllArgsConstructor
public class TeamLeadEntry implements Serializable{
    private static final long serialVersionUID = 1;
    private Long teamId;
    private String participantName;
    private String participantIdentifier;
    
}
