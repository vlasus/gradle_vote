/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.vorlon.gradle.vote.queue;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.vorlon.gradle.vote.domain.TeamLead;
import net.vorlon.gradle.vote.repo.TeamLeadsRepository;
import org.apache.log4j.Logger;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author vlasus
 */
@Component
public class VoteListener {
    Logger logger = Logger.getLogger(VoteListener.class);
    	@Autowired
	private TeamLeadsRepository repository;
    @Autowired
    private ObjectMapper objectMapper;            
    
    @RabbitListener(queues = "vote-queue")
    public void onMessage(String message) throws Exception{
        logger.info("Received from team-queue : " + message);
        Long teamId=null;
        String participantIdentifier=null;
        TeamLeadMessage teamLeadMsg=objectMapper.readValue(message,TeamLeadMessage.class);
        if(teamLeadMsg.getItem()==null){
            ObjectMapper mapper = new ObjectMapper();
            JsonNode newNode = mapper.readTree(message);
            logger.info(newNode.get("item").get("teamId"));
            teamId=Long.parseLong(newNode.get("item").get("teamId").toString());
            participantIdentifier=newNode.get("item").get("participantIdentifier").toString();
        }else{
            teamId=teamLeadMsg.getItem().getTeamId();
            participantIdentifier=teamLeadMsg.getItem().getParticipantIdentifier();
        }
        logger.info(teamLeadMsg);
        TeamLead teamLead=repository.findByTeamId(teamId);
        if(teamLead==null){
            TeamLead theTeamLead = new TeamLead(teamId, participantIdentifier);
            repository.save(theTeamLead);
        }
    }
}
