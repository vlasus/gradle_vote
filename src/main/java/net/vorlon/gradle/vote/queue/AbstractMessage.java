package net.vorlon.gradle.vote.queue;

import com.fasterxml.jackson.annotation.JsonFormat;
import static com.fasterxml.jackson.annotation.JsonFormat.Shape.STRING;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.NoArgsConstructor;

/**
 * @author vlasus
 * @param <T> Тип содержимого сообщения
 */
@NoArgsConstructor
public abstract class AbstractMessage<T> implements Serializable {
    @JsonProperty("messageId")
    protected String messageId;

    @JsonProperty("creationDate")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss", shape = STRING)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    protected LocalDateTime creationDate;

    @JsonProperty("item")
    protected T item;

    public AbstractMessage(T item) {
        this(LocalDateTime.now(), item);
    }

    public AbstractMessage(LocalDateTime dateTime, T item) {
        this.creationDate = dateTime;
        this.item = item;
    }

    public T getItem() {
        return item;
    }
    
}
