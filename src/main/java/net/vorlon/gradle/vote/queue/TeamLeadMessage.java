/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.vorlon.gradle.vote.queue;

import com.fasterxml.jackson.annotation.JsonFormat;
import static com.fasterxml.jackson.annotation.JsonFormat.Shape.STRING;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.NoArgsConstructor;

/**
 *
 * @author vlasus
 */
@NoArgsConstructor
public class TeamLeadMessage extends AbstractMessage<TeamLeadEntry> {

    public TeamLeadMessage(TeamLeadEntry entry){
        super(entry);
    }
}
