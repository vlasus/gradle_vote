/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.vorlon.gradle.vote.queue;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import java.time.LocalDateTime;
import org.apache.log4j.Logger;

/**
 *
 * @author vlasus
 */
public class LocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {
    Logger logger = Logger.getLogger(LocalDateTimeDeserializer.class);
    @Override
    public LocalDateTime deserialize(JsonParser p, DeserializationContext arg1) throws IOException, JsonProcessingException {
        LocalDateTime retVal=null;
        try{
            retVal=LocalDateTime.parse(p.getValueAsString());
        }catch(Exception e){
            logger.error(e);
        }
        return retVal;
    }
    
}
