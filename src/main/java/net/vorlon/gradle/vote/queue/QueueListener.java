/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.vorlon.gradle.vote.queue;

/**
 *
 * @author vlasus
 */
public interface QueueListener<T> {
    /**
     * Обработчик сообщений.
     *
     * @param message the message
     * @throws Exception Исключение при обработке - сообщение отправляется на повторную обработку
     */
    void onMessage(T message) throws Exception;    
}
