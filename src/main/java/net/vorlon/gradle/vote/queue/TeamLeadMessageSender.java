/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.vorlon.gradle.vote.queue;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;



/**
 *
 * @author vlasus
 */
@Component
public class TeamLeadMessageSender{
    Logger logger = Logger.getLogger(TeamLeadMessageSender.class);
    @Autowired
    AmqpTemplate template;
    
    public void send(TeamLeadMessage message) {
            ObjectMapper mapper = new ObjectMapper();
            try{
                template.convertAndSend("vote-queue", mapper.writeValueAsString(message));
            }catch(JsonProcessingException e){
                logger.error(e.getMessage());
            }
    }
    
      
}
