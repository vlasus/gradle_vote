/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.vorlon.gradle.vote.rest;

import net.vorlon.gradle.vote.domain.Participant;
import net.vorlon.gradle.vote.domain.TeamLead;
import net.vorlon.gradle.vote.queue.TeamLeadEntry;
import net.vorlon.gradle.vote.queue.TeamLeadMessage;
import net.vorlon.gradle.vote.queue.TeamLeadMessageSender;
import net.vorlon.gradle.vote.repo.ParticipantsRepository;
import net.vorlon.gradle.vote.repo.TeamLeadsRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author vlasus
 */
@RestController
@RequestMapping("team")
public class VoteController {
    Logger logger = Logger.getLogger(VoteController.class);
    @Autowired
    private ParticipantsRepository repository;
    
    @Autowired
    private TeamLeadsRepository teamRepository;

    @Autowired
    TeamLeadMessageSender sender;

    @RequestMapping("/vote/{id}/{name}")
    @ResponseBody
    String vote(@PathVariable(name = "id") Long id,
            @PathVariable(name = "name") String name) {
        logger.info("Emit to team-queue");
        Participant participant=repository.findByFirstName(name);
        TeamLeadEntry entry= new TeamLeadEntry(id,name,participant.getIdentifier());
        TeamLeadMessage msg=new TeamLeadMessage(entry);
        sender.send(msg);
        String response="send to queue";
        try{
        for(int i=0;i<10;i++){
            Thread.sleep(100); 
            TeamLead teamLead=teamRepository.findByTeamId(id);
            if(teamLead!=null){
                response="Капитан команды: "+teamLead.getParticipantIdentifier();
                break;
            }
        }
        }catch(Exception e){
            logger.error(response=e.getMessage());
        }
        return response;
    }
    
    @RequestMapping("/refuse/{id}/{name}")
    @ResponseBody
    String refuse(@PathVariable(name = "id") Long id,
            @PathVariable(name = "name") String name) {
        logger.info("Emit to team-queue");
        String response="отказ принят";
        Participant participant=repository.findByFirstName(name);
        TeamLead teamLead=teamRepository.findByTeamId(id);
        if(teamLead!=null){
            if(teamLead.getParticipantIdentifier().equalsIgnoreCase(participant.getIdentifier())){
                teamRepository.delete(teamLead);
            }else{
                response="Капитан команды: "+teamLead.getParticipantIdentifier();
            }
        }
        return response;
    }    
}
