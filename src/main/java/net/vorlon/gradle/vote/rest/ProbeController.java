/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.vorlon.gradle.vote.rest;

import org.apache.log4j.Logger;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author vlasus
 */
@RestController
@RequestMapping("queue")
public class ProbeController {
    Logger logger = Logger.getLogger(ProbeController.class);

    @Autowired
    AmqpTemplate template;

    @RequestMapping("/probe")
    @ResponseBody
    String queue1() {
        logger.info("Emit to test-queue");
        template.convertAndSend("test-queue","Message to queue");
        template.convertAndSend("vote-queue","{\"messageId\":null,"
                + "\"creationDate\":null,\"item\":{\"teamId\":1,\"participantName\":\"Вася\"}}");
        return "Emit to queue";
    }    
}
