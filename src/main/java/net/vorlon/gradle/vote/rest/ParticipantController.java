/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.vorlon.gradle.vote.rest;

import java.util.List;
import net.vorlon.gradle.vote.domain.Participant;
import net.vorlon.gradle.vote.repo.ParticipantsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author vlasus
 */
@RestController
@RequestMapping("repo")
public class ParticipantController {
    	@Autowired
	private ParticipantsRepository repository;
        
    @RequestMapping("/init")
    public void init(){

		// save a couple of customers
		repository.save(new Participant("Маша", "Милова"));
		repository.save(new Participant("Вася", "Васин")); 
                
    }
    @RequestMapping(value = "/find-all", method = RequestMethod.GET)
    public List<Participant> findAll(){
    return repository.findAll();
}
    
}
