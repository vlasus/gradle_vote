/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.vorlon.gradle.vote.repo;

import java.util.List;
import net.vorlon.gradle.vote.domain.Participant;
import net.vorlon.gradle.vote.domain.TeamLead;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *
 * @author vlasus
 */
public interface TeamLeadsRepository extends MongoRepository<TeamLead, String> {

    public TeamLead findByTeamId(Long teamId);
    
}
