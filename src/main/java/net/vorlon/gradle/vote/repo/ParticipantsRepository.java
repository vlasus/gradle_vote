/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.vorlon.gradle.vote.repo;

import java.util.List;
import net.vorlon.gradle.vote.domain.Participant;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *
 * @author vlasus
 */
public interface ParticipantsRepository extends MongoRepository<Participant, String> {

    public Participant findByFirstName(String firstName);
    public List<Participant> findByLastName(String lastName);
    
}
